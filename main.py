from flask import Flask, request
from fibo import fib


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
    )

    @app.route('/', methods=('GET', 'POST'))
    def challenge():
        if request.method == 'POST':
            params = abs(request.form['index'])
            fi = fib(params+1)
            return str(fi[params])
        else:
            return 'Hello, World!'

    return app

