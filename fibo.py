def fib(index):
    a, b = 0,1
    resultado = []
    while True:
        resultado.append(a)
        a, b = b, a+b
        if len(resultado) == index:
            break
    return resultado